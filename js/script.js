/* Set the width of the side navigation to 250px and the left margin of the page content to 250px */
let menuBtn = document.getElementById('menu-btn');
let closeBtn = document.getElementById('closeBtn');
//
//let contentLoad = document.getElementsByClassName('content-box');

function openNav() {
    document.getElementById('mySidenav').style.width = '250px';
    document.getElementById('mySidenav').style.opacity = '1';
    document.getElementById('main').style.marginLeft = '250px';
    document.getElementById('menu-btn').style.visibility = 'hidden';
}

/* Set the width of the side navigation to 0 and the left margin of the page content to 0 */
function closeNav() {
    document.getElementById('mySidenav').style.width = '0';
    document.getElementById('main').style.marginLeft = '0';
    document.getElementById('menu-btn').style.visibility = 'visible';
}

//
function openContent() {
    document.getElementById('contentBox').style.opacity = '1';
}

// //Typewriter
// var aText = new Array(
//     'Cześć',
//     'Jestem Marcin Świrek',
//     'I jestem Junior Front-end Developerem'
// );
// var iSpeed = 100; // time delay of print out
// var iIndex = 0; // start printing array at this posision
// var iArrLength = aText[0].length; // the length of the text array
// var iScrollAt = 20; // start scrolling up at this many lines

// var iTextPos = 0; // initialise text position
// var sContents = ''; // initialise contents variable
// var iRow; // initialise current row

// function typewriter() {
//     sContents = ' ';
//     iRow = Math.max(0, iIndex - iScrollAt);
//     var destination = document.getElementById('typedText');

//     while (iRow < iIndex) {
//         sContents += aText[iRow++] + '<br />';
//     }
//     destination.innerHTML =
//         sContents + aText[iIndex].substring(0, iTextPos) + '_';
//     if (iTextPos++ == iArrLength) {
//         iTextPos = 0;
//         iIndex++;
//         if (iIndex != aText.length) {
//             iArrLength = aText[iIndex].length;
//             setTimeout('typewriter()', 500);
//         }
//     } else {
//         setTimeout('typewriter()', iSpeed);
//     }
// }

// typewriter();

// Typewriter ends

menuBtn.addEventListener('click', openNav);
closeBtn.addEventListener('click', closeNav);
document.addEventListener('DOMContentLoaded', openNav);
document.addEventListener('DOMContentLoaded', openContent);
